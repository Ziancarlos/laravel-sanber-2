<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/table', function(){
    return view('table');
});

Route::get('/data-tables', function() {
    return view('data-table');
});


Route::get('/cast', "CastsController@index");
Route::get('/cast/create', "CastsController@create");
Route::post('/cast', "CastsController@store");
Route::get('/cast/{cast}',"CastsController@show");
Route::get('/cast/{cast}/edit', "CastsController@edit");
Route::patch('/cast/{cast}', "CastsController@update");
Route::delete('/cast/{cast}', "CastsController@destroy");