@extends('templates.layout')



@section('content')
     <div class="container mt-4">
        <form action="/cast/{{$cast->id}}" method="post">
            @method("patch")
            @csrf
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Nama:</label>
                <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="nama" value="{{$cast->nama}}">
                
            </div>

            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">Umur:</label>
                <input type="text" class="form-control" id="exampleInputPassword1" name="umur" value="{{$cast->umur}}">
            </div>
            
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label">Bio:</label>
                <input type="text" class="form-control" id="exampleInputPassword1" name="bio" value="{{$cast->bio}}">
            </div>
          
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
     </div>
@endsection