@extends('templates.layout')


@section('content')
    <div class="container">
        <a href="/cast/create" class="btn btn-primary">Tambah Data</a>
        <div class="row">
            <table class="table">
                    <thead>
                        <tr>
                        <th scope="col">id</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Umur</th>
                        <th scope="col">bio</th>
                        <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($casts as $cast)
                            <tr>
                                <th scope="row">{{$loop->iteration}}</th>
                                <td>{{$cast->nama}}</td>
                                <td>{{$cast->umur}}</td>
                                <td>{{$cast->bio}}</td>
                                <td>
                                    <a href="/cast/{{$cast->id}}" class="btn btn-primary">Info</a>
                                <form action="/cast/{{$cast->id}}" method="post" class="d-inline">
                                    @method('delete')
                                    @csrf
                                    <button href="" class="btn btn-danger">Delete</button>
                                </form>
                                </td>
                            </tr>
                        @endforeach
                        
                        
                    </tbody>
            </table>
        </div>
    </div>
@endsection