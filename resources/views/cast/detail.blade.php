@extends('templates.layout')


@section('content')
    <div class="container mt-4">
        <div class="card" style="width: 500px; heigth: 500px;">
    <div class="card-body">
        <h5 class="card-title">{{$cast->nama}}</h5>
        <h6 class="card-subtitle mb-2 text-muted">{{$cast->umur}}</h6>
        <p class="card-text">{{$cast->bio}}</p>
        <a href="/cast/{{$cast->id}}/edit" class="card-link">Edit</a>
        
    </div>
</div>
    </div>
@endsection